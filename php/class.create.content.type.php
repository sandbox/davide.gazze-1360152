<?php

// Creating the content type
class Create_content_type {

    // the content created
    private $content;

    public function create_content_type($operation_name, $operation, $fields, $label, $default_values = null) {
        $weight = 0;
        $name_cck = $operation_name;
        $type_cck = strtolower($operation_name);
        $description = "A RemoteCCK content type for the operation ".$operation;
        $this->remotecck_create_new_content_type($operation, $name_cck, $type_cck, $description, &$weight);
        $num_of_fields = count($fields);
        for($i=0 ; $i<$num_of_fields; $i++) {
            $field_name = $fields[$i]["field_name"];
            $direction = $fields[$i]["direction"];
            $group = ($direction=="IN") ? "group_request" : "group_response";
            $field_type = $fields[$i]["field_type"];
            $default = (isset($default_values[$i])) ? $default_values[$i] : "";
            // The code for different type
            switch ($field_type) {
                case "float":
                    $this->content['fields'][$i] = $this->remotecck_create_float_field_cck($label[$i], $field_name, $group, &$weight, $default);
                    break;
                case "double":
                    $this->content['fields'][$i] = $this->remotecck_create_float_field_cck($label[$i], $field_name, $group, &$weight, $default);
                    break;
                case "int":
                    $this->content['fields'][$i] = $this->remotecck_create_int_field_cck($label[$i], $field_name, $group, &$weight, $default);
                    break;
                case "decimal":
                    $this->content['fields'][$i] = $this->remotecck_create_decimal_field_cck($label[$i], $field_name, $group, &$weight, $default);
                    break;
                default:
                    $this->content['fields'][$i] = $this->remotecck_create_text_field_cck($label[$i], $field_name, $group, &$weight, $default);
                    break;
            }
            
        }
    }

    public function getContent() {
        // return the content created
        return $this->content;
    }

    /******************************************************************************************
     *                                   Private Functions
     ******************************************************************************************/

    private function remotecck_create_new_content_type($operation, $name_cck, $type_cck, $description, &$weight) {
        $weight = 0;
        $this->content['type']  = array (
          'name' => $name_cck,
          'type' => $type_cck,
          'description' => $description,
          'title_label' => 'Title',
          'body_label' => 'Body',
          'min_word_count' => '0',
          'help' => '',
          'node_options' => 
          array (
            'status' => true,
            'promote' => true,
            'sticky' => false,
            'revision' => false,
          ),
          'old_type' => 'content_type_list',
          'orig_type' => '',
          'module' => 'node',
          'custom' => '1',
          'modified' => '1',
          'locked' => '0',
          'comment' => '2',
          'comment_default_mode' => '4',
          'comment_default_order' => '1',
          'comment_default_per_page' => '50',
          'comment_controls' => '3',
          'comment_anonymous' => 0,
          'comment_subject_field' => '1',
          'comment_preview' => '1',
          'comment_form_location' => '0',
        );
        $this->content['groups']  = array (
          0 => 
          array (
            'label' => 'Request',
            'group_type' => 'standard',
            'settings' => 
            array (
              'form' => 
              array (
                'style' => 'fieldset',
                'description' => '',
              ),
              'display' => 
              array (
                'description' => '',
                'teaser' => 
                array (
                  'format' => 'fieldset',
                  'exclude' => 0,
                ),
                'full' => 
                array (
                  'format' => 'fieldset',
                  'exclude' => 0,
                ),
                4 => 
                array (
                  'format' => 'fieldset',
                  'exclude' => 0,
                ),
                'label' => 'above',
              ),
            ),
            'weight' => $weight++,
            'group_name' => 'group_request',
          ),
          1 => 
          array (
            'label' => 'Response',
            'group_type' => 'standard',
            'settings' => 
            array (
              'form' => 
              array (
                'style' => 'fieldset',
                'description' => '',
              ),
              'display' => 
              array (
                'description' => '',
                'teaser' => 
                array (
                  'format' => 'fieldset',
                  'exclude' => 0,
                ),
                'full' => 
                array (
                  'format' => 'fieldset',
                  'exclude' => 0,
                ),
                4 => 
                array (
                  'format' => 'fieldset',
                  'exclude' => 0,
                ),
                'label' => 'above',
              ),
            ),
            'weight' => $weight++,
            'group_name' => 'group_response',
          ),
        );
        $this->content['extra']  = array (
          'title' => '-5',
          'body_field' => '-3',
          'revision_information' => '-2',
          'comment_settings' => '-1',
          'menu' => '-4',
        );
        $this->content['fields'] = array();
    }

    private function remotecck_create_text_field_cck($field_label, $field_name, $group, &$weight, $default) {
        // create a text field
        $field = array (
                'label' => $field_label,
                'field_name' => $field_name,
                'type' => 'text',
                'widget_type' => 'text_textfield',
                'change' => 'Change basic information',
                'weight' => $weight,
                'rows' => 5,
                'size' => '60',
                'description' => '',
                'default_value' => 
                array (
                  0 => 
                  array (
                    'value' => $default,
                    '_error_element' => 'default_value_widget][$field_name][0][value',
                  ),
                ),
                'default_value_php' => '',
                'default_value_widget' => 
                array (
                  'field_out_string' => 
                  array (
                    0 => 
                    array (
                      'value' => $default,
                      '_error_element' => 'default_value_widget][$field_name][0][value',
                    ),
                  ),
                ),
                'group' => $group,
                'required' => 0,
                'multiple' => '0',
                'text_processing' => '0',
                'max_length' => '',
                'allowed_values' => '',
                'allowed_values_php' => '',
                'op' => 'Save field settings',
                'module' => 'text',
                'widget_module' => 'text',
                'columns' => 
                array (
                  'value' => 
                  array (
                    'type' => 'text',
                    'size' => 'big',
                    'not null' => false,
                    'sortable' => true,
                    'views' => true,
                  ),
                ),
                'display_settings' => 
                array (
                  'weight' => $weight++,
                  'parent' => $group,
                  'label' => 
                  array (
                    'format' => 'above',
                  ),
                  'teaser' => 
                  array (
                    'format' => 'default',
                    'exclude' => 0,
                  ),
                  'full' => 
                  array (
                    'format' => 'default',
                    'exclude' => 0,
                  ),
                  4 => 
                  array (
                    'format' => 'default',
                    'exclude' => 0,
                  ),
                )
            );
        return $field;
    }

    private function remotecck_create_int_field_cck($field_label, $field_name, $group, &$weight, $default) {
        // create an int field
        $field  = array (
                'label' => $field_label,
                'field_name' => $field_name,
                'type' => 'number_integer',
                'widget_type' => 'number',
                'change' => 'Change basic information',
                'weight' => $weight,
                'description' => '',
                'default_value' => 
                array (
                  0 => 
                  array (
                    'value' => $default,
                    '_error_element' => 'default_value_widget][$field_name][0][value',
                  ),
                ),
                'default_value_php' => '',
                'default_value_widget' => NULL,
                'group' => $group,
                'required' => 0,
                'multiple' => '0',
                'min' => '',
                'max' => '',
                'prefix' => '',
                'suffix' => '',
                'allowed_values' => '',
                'allowed_values_php' => '',
                'op' => 'Save field settings',
                'module' => 'number',
                'widget_module' => 'number',
                'columns' => 
                array (
                  'value' => 
                  array (
                    'type' => 'int',
                    'not null' => false,
                    'sortable' => true,
                  ),
                ),
                'display_settings' => 
                array (
                  'weight' => $weight++,
                  'parent' => $group,
                  'label' => 
                  array (
                    'format' => 'above',
                  ),
                  'teaser' => 
                  array (
                    'format' => 'default',
                    'exclude' => 0,
                  ),
                  'full' => 
                  array (
                    'format' => 'default',
                    'exclude' => 0,
                  ),
                  4 => 
                  array (
                    'format' => 'default',
                    'exclude' => 0,
                  ),
                ),
        );
        return $field;
    }

    private function remotecck_create_float_field_cck($field_label, $field_name, $group, &$weight, $default) {
        // create a float field
        $field  = array (
                'label' => $field_label,
                'field_name' => $field_name,
                'type' => 'number_float',
                'widget_type' => 'number',
                'change' => 'Change basic information',
                'weight' => $weight,
                'description' => '',
                'default_value' => 
                array (
                    0 => 
                    array (
                        'value' => $default,
                        '_error_element' => 'default_value_widget][$field_name][0][value',
                    ),
                ),
                'default_value_php' => '',
                'default_value_widget' => NULL,
                'group' => $group,
                'required' => 0,
                'multiple' => '0',
                'min' => '',
                'max' => '',
                'prefix' => '',
                'suffix' => '',
                'allowed_values' => '',
                'allowed_values_php' => '',
                'op' => 'Save field settings',
                'module' => 'number',
                'widget_module' => 'number',
                'columns' => 
                    array (
                      'value' => 
                      array (
                        'type' => 'float',
                        'not null' => false,
                        'sortable' => true,
                      ),
                ),
                'display_settings' => 
                    array (
                      'weight' => $weight++,
                      'parent' => $group,
                      'label' => 
                      array (
                        'format' => 'above',
                      ),
                      'teaser' => 
                      array (
                        'format' => 'default',
                        'exclude' => 0,
                ),
                'full' => 
                      array (
                        'format' => 'default',
                        'exclude' => 0,
                ),
                4 => 
                      array (
                        'format' => 'default',
                        'exclude' => 0,
                ),
            )
        );
        return $field;
    }

    private function remotecck_create_decimal_field_cck($field_label, $field_name, $group, &$weight, $default) {
        // create a decimal field
        $field  = array (
                'label' => $field_label,
                'field_name' => $field_name,
                'type' => 'number_decimal',
                'widget_type' => 'number',
                'change' => 'Change basic information',
                'weight' => $weight,
                'description' => '',
                'default_value' => 
                array (
                    0 => 
                    array (
                        'value' => $default,
                        '_error_element' => 'default_value_widget][$field_name][0][value',
                    ),
                ),
                'default_value_php' => '',
                'default_value_widget' => NULL,
                'group' => $group,
                'required' => 0,
                'multiple' => '0',
                'min' => '',
                'max' => '',
                'precision' => '10',
                'scale' => '2',
                'decimal' => '.',
                'prefix' => '',
                'suffix' => '',
                'allowed_values' => '',
                'allowed_values_php' => '',
                'op' => 'Save field settings',
                'module' => 'number',
                'widget_module' => 'number',
                'columns' => 
                    array (
                        'value' => 
                            array (
                                'type' => 'numeric',
                                'precision' => '10',
                                'scale' => '2',
                                'not null' => false,
                                'sortable' => true,
                            ),
                    ),
                    'display_settings' => 
                        array (
                            'weight' => $weight++,
                            'parent' => $group,
                            'label' => 
                                array (
                                    'format' => 'above',
                            ),
                            'teaser' => 
                                array (
                                    'format' => 'default',
                                    'exclude' => 0,
                            ),
                            'full' => 
                                array (
                                    'format' => 'default',
                                    'exclude' => 0,
                            ),
                            4 => 
                                array (
                                    'format' => 'default',
                                    'exclude' => 0,
                            ),
                        ),
            );
        return $field;
    }
}

?>