
function get_operation(id_wsdl, id_insert_operation, id_append, id_select_operation, id_append_fields, id_select_fields, name_of_cck, id_field_name, check_used, is_new) {
    // Get the operation of WSDL
    wsdl_name = $("#"+id_wsdl).val();
    data_send = "wsdl="+wsdl_name+"&check_used="+check_used;
    $.ajax({type: "POST", url:"?q=remotecck.operation.wsdl", async:true, data:data_send, success:function(data) {
        $('#'+id_append_fields).empty();
        operation_json = eval('(' + data + ')');
        number_of_operation = $(operation_json).length;
        // Use Jquery to get select list element
        if($("#"+id_select_operation).val()==undefined) {
            select_code = "<select id='"+id_select_operation+"' onChange=\"if(this.selectedIndex!=0) { op_name_new = new String(this.options[this.selectedIndex].text); do { op_name_old = op_name_new; op_name_new = op_name_old.replace('.','_'); } while(op_name_old != op_name_new); $('operation-wsdl').val(this.options[this.selectedIndex].text); } get_operation_fields(\'"+id_wsdl+"\',\'"+id_select_operation+"\',\'"+id_append_fields+"\',\'"+id_select_fields+"\',\'"+id_insert_operation+"\',\'"+id_field_name+"\');\"/>";
            $("#"+id_append).append("<b>Operations of Web Service:</b><br/>");
            $("#"+id_append).append(select_code);
        }
        else {
            // remove all
            $("#"+id_select_operation).empty();
        }
        operation_list = $("#"+id_select_operation);
        for(i=0; i<number_of_operation; i++) {
            // append the new element
            option = new Option(operation_json[i], operation_json[i]);
            operation_list.append(option);
        }
        // Empty the name of cck
        //$('#'+name_of_cck).val("");
    }});
}

function get_operation_fields(id_wsdl, id_operation, id_append, id_fields, id_insert_operation, id_field_name) {
    // Get the fields of operation of WSDL
    wsdl_name = $("#"+id_wsdl).val();
    operation_name = $("#"+id_operation).val();
    $("#"+id_insert_operation).val(operation_name);
    data_send = "wsdl="+wsdl_name+"&operation="+operation_name;
    $.ajax({type: "POST", url:"?q=remotecck.fields.operation.wsdl", async:true, data:data_send, success:function(data) {
        fields_json = eval('(' + data + ')');
        number_of_fields = $(fields_json).length;
        // Use Jquery to insert the fields
        fields_operation_list = $("#"+id_append);
        fields_operation_list.empty();
        // Insert the number of fields
        text_code = "<input type='hidden' id='"+id_fields+"' name='"+id_fields+"' value='"+number_of_fields+"'/>";
        fields_operation_list.append(text_code);
        for(i=0; i<number_of_fields; i++) {
            // append the new element
            text_code = "<input type='hidden' size='62' id='"+id_fields+i+"' name='"+id_fields+i+"' value='"+fields_json[i].field_name+"'/>";
            fields_operation_list.append(text_code);
            text_code = "<input type='hidden' size='62' id='"+id_field_name+i+"' name='"+id_field_name+i+"' value='"+fields_json[i].field_name+"'/>";
            fields_operation_list.append(text_code);
        }
    }});
}
