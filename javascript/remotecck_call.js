
function get_fields_json(content_type, field_json_str, operation_name, id_operation, is_async) {
    // Get the information of fields of Content Type
    field_json = eval('(' + field_json_str + ')');
    num_input_parameter = field_json.num_input_param;
    value_array = new Array();
    for(i=0; i<num_input_parameter; i++) {
        // take the input parameter
        id = get_id_of_cck_field(field_json.items.input[i]);
        value_array[i] = $(id).val();
    }
    // perform the call
    make_call_js(content_type, value_array, operation_name, id_operation, is_async);
}

function make_call_js(content_type, value_array, operation_name, id_operation, is_async) {
    // Make the call
    // and set the web page
    data_send = "operation_name="+operation_name+"&id_operation="+id_operation+"&data="+value_array.join(",")+"&content_type="+content_type;
    $.ajax({type: "POST", url:"?q=remotecck.call", async:is_async, data:data_send, success:function(data) {
        // Create the response ID
        // I suppose that is an input field
        var field_json = eval('(' + data + ')');
        for(i=0; i<field_json.num_output_param; i++) {
            id = get_id_of_cck_field(field_json[i].name);
            $(id).val(field_json[i].value); 
        }
    }});
}

function get_id_of_cck_field(name_of_field) {
    // Return the information of the cck field
    // for the different type of html
    // input,
    // I suppose that is an input field
    id = "#edit-"+name_of_field.replace("_","-")+"-0-value";
    // the select for has different name
    if($(id).val()==undefined) {
        // id for select field
        id = "#edit-"+name_of_field.replace("_","-")+"-value";
    }
    return id;
}

function manage_save_request_mode(id_submit, radio_name, id, checkbox_name) {
    // Insert the onSubmit function on sava button
    handler = function() {
        // It's writtend in this form because in other it doesn't run
        // with sync request
        alter_request_mode(radio_name, checkbox_name, id, false);
    };
    // Bind the event
    $("#"+id_submit).bind("submit", handler);
}

function alter_request_mode(radio_name, checkbox_name, id, is_async) {
    // Save the insert mode
    // If Checkbox is checked the insert mode is 4
    insert_mode = 4;
    if(!$(checkbox_name).attr('checked')) {
        insert_mode = $("input[@name="+radio_name+"]:checked").val();
    }
    data_send = "id="+id+"&insert_mode="+insert_mode;
    $.ajax({type: "POST", url:"?q=remotecck.request.mode", async:is_async, data:data_send, success:function(data) {
        // Modify the save mode
    }});
}

function insert_request_mode(insert_mode, id_append, node_form, content_type, field_json_str, operation_name, id_operation) {
    // Insert mode menu
    handler = function() {
        // It's writtend in this form because in other it doesn't run
        // with sync request
        get_fields_json(content_type, field_json_str, operation_name, id_operation, false);
    };
    switch(insert_mode) {
        case 0: {
                    // No Call
                    // Do nothings right now
                    get_fields_json(content_type, field_json_str, operation_name, id_operation, true);
                    break;
        }
        case 1: {
                    // After Submit
                    // Bind the event
                    $("#"+node_form).bind("submit", handler);
                    break;
        }
    }
}

function set_readonly_fields(field_json_str) {
    // Set readonly all the field
    var field_json = eval('(' + field_json_str + ')');
    num_output_parameter = field_json.num_output_param;
    for(i=0; i<num_output_parameter; i++) {
        // take the input parameter
        id_field = get_id_of_cck_field(field_json.items.output[i]);
        $(id_field).attr("readonly", true);
    }
}
