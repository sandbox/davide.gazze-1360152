/* $Id: README.txt,v 0.1 2011/03/13 17:30:00 sun Exp $ */

-- REQUIREMENTS --

RemoteCCK requires the NuSOAP library to work correctly.


-- INSTALLATION --

To install RemoteCCK, first, you must download the NuSOAP library from:

                http://nusoap.sourceforge.net/

Then you must unpack the library and copy it in the directory "lib" of remotecck module, after that you can install the module as usual.

-- SUMMARY --

RemoteCCK performs a link between a generic web service and a content type.

The module can:
1) Analyze a web service
2) Create a new content type to invoke an operation of a web service
3) Update an existing content type to invoke an operation of a web service
4) Invoke an operation of a web service from the che "create content" menu

After the installation, at the path:

        admin/settings/remotecck

you have the configuration menu.

First of all you must insert a valid web service url, at the path:

        admin/settings/remotecck/insert

After that go to:

        admin/settings/remotecck/operation
       
to create a new content type for this operation, it will create a connection between the operation selected and the content type.

To update an existing content type (adding the field to invoke operation) go to:

        admin/settings/remotecck/update

To perform the manual mapping between field of a content type and operation go to:

        admin/settings/remotecck/mapping

To remove the connection between a content type and an operation go to:

        admin/settings/remotecck/delete

In the configuration form of a content type (with associated an operation) it's possible to set the request type of the remoteCCK call.

There are four types of call:
1) Before Submit  : the operation is invoked when the node is created
2) After Submit   : the operation is invoked after the click of submit button
3) During Compile : a "make call" button is added and the operation is invoked with the click of that button
4) Do Nothing     : do not perform the call

If a content type has associated more operations, a select area will appear for every operation.
You can select the check "One Request Button for all operation" for having an unique "make call" button for every operation.
