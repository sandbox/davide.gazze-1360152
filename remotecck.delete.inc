<?php
// insert WSDL

function remotecck_delete(&$form_state = NULL) {
    global $user;
    if($user->uid != 1) {
        // only the admin can use remotecck
        drupal_set_title('Access denied');
        drupal_set_message("You are not authorized to access this page.","error");
        return "";
    }
    // making the form
    $form = array();
    $connection_created = remotecck_getting_insert_cck();
    $connection_created_name = array();
    $num_of_connection = count($connection_created);
    $connection_created_name[0] = 'Select options';
    for($i=0; $i<$num_of_connection; $i++) {
        $name_content_type = $connection_created[$i]['content_type'];
        $connection_created_name[$name_content_type] = $name_content_type;
    }
    // insert the content type
    $name_of_cck = 'edit-name-of-cck';
    $name_of_div = 'connection_div';
    $name_of_select = 'connection_select';
    $connection_select = 'edit-connection-select';
    $form['name'] = array(
        '#type'             => 'select',
        '#id'               => $name_of_cck,
        '#weight'           => $weight++,
        '#title'            => t('Name of Content Type'),
        '#options'          => $connection_created_name,
        '#description'      => t("If you don't have any Content Type click <a href=!url>here</a>.", array('!url' => url("admin/content/types/add", array('absolute' => TRUE)))),
        '#attributes'       => array(
            'onchange'           => "if(this.selectedIndex!=0) { get_information_for_content_type('$name_of_cck', '$name_of_div', '$name_of_select', '$connection_select'); } else { $('#$name_of_div').empty(); }")
    );
    $form['connection'] = array(
        '#type'             => 'item',
        '#weight'           => $weight++,
        '#prefix'           => "<div id='$name_of_div'>",
        '#suffix'           => '</div>'
    );
    $form['connection_select'] = array(
        '#type'             => 'hidden',
        '#weight'           => $weight++
    );
    $form['submit'] = array(
        '#type'             => 'submit',
        '#weight'           => $weight++,
        '#value'            => t('Delete Operation')
    );
    return $form;
}

function remotecck_delete_submit($form, &$form_state) {
    // Submit the Content Type
    $content_type = $form_state['values']['name'];
    $link_str = $form_state['values']['connection_select'];
    $link_array = explode(' (', $link_str);
    $link_array[1] = substr($link_array[1],0,-1);
    $value = remotecck_delete_connection($content_type, $link_array[0], $link_array[1]);
    if($value==1) drupal_set_message("Connection <b>$link_str</b> from <b>$content_type</b> delete");
    else form_set_error("Can't delete the <b>$link_str</b> from <b>$content_type</b>");
}

?>
