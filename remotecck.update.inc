<?php
// insert WSDL

function remotecck_update(&$form_state = NULL) {
    global $user;
    if($user->uid != 1) {
        // only the admin can use remotecck
        drupal_set_title('Access denied');
        drupal_set_message("You are not authorized to access this page.","error");
        return "";
    }
    // making the form
    $form = array();
    // insert the content type
    $content_types = content_copy_types();
    $form['name_of_cck'] = array(
        '#type'             => 'select',
        '#weight'           => $weight++,
        '#title'            => t('Name of Content Type'),
        '#options'          => $content_types,
        '#description'      => t("If you don't have any Content Type click <a href=!url>here</a>.", array('!url' => url("admin/content/types/add", array('absolute' => TRUE))))
    );
    // getting the list of WSDL
    $wsdl_list[0] = 'Select options';
    $wsdl_list = array_merge($wsdl_list, remotecck_get_WSDL());
    $form['wsdl'] = array(
        '#type'             => 'select',
        '#weight'           => $weight++,
        '#title'            => t('Web Services List'),
        '#options'          => $wsdl_list,
        '#description'      => t("If you don't have any Web Services click <a href=!url>here</a>.", array('!url' => url("admin/settings/remotecck/insert", array('absolute' => TRUE)))),
        '#attributes'       => array(
            'onchange'           => 'get_operation("edit-wsdl","edit-operation-wsdl","operation_wsdl_div","edit_operation_select","fields_operation_wsdl_div","edit_field_operation","edit-name-of-cck","edit_field_name", 0, false);')
    );
    $form['operation_wsdl_insert'] = array(
        '#type'             => 'item',
        '#weight'           => $weight++,
        '#prefix'           => '<div id="operation_wsdl_div">',
        '#suffix'           => '</div>'
    );
    $form['operation_wsdl'] = array(
        '#type'             => 'hidden',
        '#weight'           => $weight++
    );
    $form['fields_operation_wsdl'] = array(
        '#type'             => 'hidden',
        '#weight'           => $weight++,
        '#prefix'           => '<div id="fields_operation_wsdl_div">',
        '#suffix'           => '</div>'
    );
    if(isset($form_state['post']['edit_field_operation'])) {
        $num_fields = $form_state['post']['edit_field_operation'];
        // saving the post information
        if($num_fields>0) {
            $form['edit_field_operation'] = array(
                '#type'             => 'hidden',
                '#value'            => $form_state['post']['edit_field_operation']
            );
            for($i=0; $i<$num_fields; $i++) {
                // Insert field
                $form['edit_field_operation'.$i] = array(
                    '#type'             => 'hidden',
                    '#value'            => $form_state['post']['edit_field_operation'.$i]
                );
                $form['edit_field_name'.$i] = array(
                    '#type'             => 'hidden',
                    '#value'            => $form_state['post']['edit_field_name'.$i]
                );
            }
        }
    }
    $form['submit'] = array(
        '#type'             => 'submit',
        '#weight'           => $weight++,
        '#value'            => t('Update Content Type')
    );
    return $form;
}

function remotecck_update_validate($form, &$form_state) {
    // Validating the fields name
    $num_fields = $form_state['values']['edit_field_operation'];
    for($i=0; $i<$num_fields; $i++) {
        // Check for duplicate name of file
        $error = false;
        $fields_insert = $form_state['values']['edit_field_operation'.$i];
        $field_name = "field_".strtolower($fields_insert);
        $field_name = remotecck_validate_field_name($field_name);
        // Saving the fieldname
        $form_state['values']['edit_field_name'.$i] = $field_name;
    }
}

function remotecck_update_submit($form, &$form_state) {
    // Submit the Content Type
    remotecck_operation_mapping($form, &$form_state, false);
}

?>
