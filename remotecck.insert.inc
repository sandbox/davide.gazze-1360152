<?php
// insert WSDL

function remotecck_insert(&$form_state = NULL) {
    // Check for the $user
    global $user;
    if($user->uid != 1) {
        // only the admin can use remotecck
        drupal_set_title('Access denied');
        drupal_set_message("You are not authorized to access this page.","error");
        return "";
    }
    // retrieve the submitted information
    $weight = 0;
    $url_analyzed = "";
    $operation_analyzed = array();
    $fields_in_analyzed = array();
    $fields_out_analyzed = array();
    $fields_visualize = false;
    if(isset($form_state['response']['url'])) $url_analyzed = $form_state['response']['url'];
    if(isset($form_state['response']['operation'])) $operation_analyzed = $form_state['response']['operation'];
    if(isset($form_state['response']['fields_in'])) $fields_in_analyzed = $form_state['response']['fields_in'];
    if(isset($form_state['response']['fields_out'])) {
        $fields_out_analyzed = $form_state['response']['fields_out'];
        $fields_visualize = true;
    }
    // making the form
    $form = array();
    $form['url'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('Web Service URL'),
        '#size' => 85
    );
    $form['advance_fieldset'] = array(
        '#title' => t('Advance Information'),
        '#weight' => $weight++,
        '#type' => 'fieldset',
        '#collapsible' => '1',
        '#collapsed' => '1'
    );
    $form['advance_fieldset']['soap_version'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('SOAP VERSION')
    );
    $form['advance_fieldset']['login'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('LOGIN')
    );
    $form['advance_fieldset']['password'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('PASSWORD')
    );
    $form['advance_fieldset']['proxy_host'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('PROXY HOST')
    );
    $form['advance_fieldset']['proxy_port'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('PROXY PORT')
    );
    $form['advance_fieldset']['proxy_login'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('PROXY LOGIN')
    );
    $form['advance_fieldset']['proxy_password'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('PROXY PASSWORD')
    );
    $form['advance_fieldset']['local_cert'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('LOCAL CERTIFICATE')
    );
    $form['advance_fieldset']['compression'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('COMPRESSION')
    );
    $form['advance_fieldset']['encoding'] = array(
        '#weight' => $weight++,
        '#type' => 'textfield',
        '#title' => t('ENCODING')
    );
    if($fields_visualize) {
        $form['table_field'] = array(
            '#title' => t('Web Service Analyze'),
            '#weight' => $weight++,
            '#type' => 'fieldset',
            '#collapsible' => '1',
            '#collapsed' => '0'
        );
        $form['table_field']['fields_in'] = array(
          '#weight' => $weight++,
          '#type' => 'item',
          '#value' => remotecck_create_fields_table('Input messages', $fields_in_analyzed)
        );
        $form['table_field']['fields_out'] = array(
          '#weight' => $weight++,
          '#type' => 'item',
          '#value' => remotecck_create_fields_table('Output messages', $fields_out_analyzed)
        );
    }
    $form['submit'] = array(
        '#type' => 'submit',
        '#weight' => $weight++,
        '#value' => t('Analyze')
    );
    return $form;
}

function remotecck_validate($form, &$form_state) {
    // Do nothing right now
}

function remotecck_insert_submit($form, &$form_state) {
    $url = $form_state['values']['url'];
    $soap_version = $form_state['values']['soap_version'];
    $login = $form_state['values']['login'];
    $password = $form_state['values']['password'];
    $proxy_host = $form_state['values']['proxy_host'];
    $proxy_port = $form_state['values']['proxy_port'];
    $proxy_login = $form_state['values']['proxy_login'];
    $proxy_password = $form_state['values']['proxy_password'];
    $local_cert = $form_state['values']['local_cert'];
    $compression = $form_state['values']['compression'];
    $encoding = $form_state['values']['encoding'];
    //***************************************
    // Insert the WSDL into database
    drupal_set_message(t('WSDL URL ANALYZED: %url',array('%url' => $url)));
    $wsdl = new Analyzer($url);
    $err = $wsdl->getError();
    if($err == 1) {
        $type = "soap";
        $operation = $wsdl->getOperations("");
        $dup = remotecck_insertWSDL($url, $soap_version, $login, $password, $proxy_host, $proxy_port, $proxy_login, $proxy_password, $local_cert, $compression, $encoding);
        //$dup = -1;
        if($dup==0) remotecck_insertOperation($url,$operation,$type);
        for($i=0; $i<count($operation); $i++) {
            // insert field for alla operation
            // check for empty message 
            if(($operation[$i]["input"]["messageName"]!="") && ($operation[$i]["output"]["messageName"]!="")){
                $fields_in[$i] = $wsdl->getTypes($operation[$i]["operation"],$operation[$i]["input"]);
                if($dup==0) remotecck_insertFields($fields_in[$i]);
                $fields_out[$i] = $wsdl->getTypes($operation[$i]["operation"],$operation[$i]["output"]);
                if($dup==0) remotecck_insertFields($fields_out[$i]);
            }
        }
        if($dup==0) drupal_set_message("Insert WDSL Done");
        else drupal_set_message("WDSL URL PRESET ON DATABASE");
        // Saving the URL, Operation and Field of WSDL
        // Submit the form using these values.
        $form_state['rebuild'] = TRUE;
        $form_state['response']['url'] = $url;
        $form_state['response']['operation'] = $operation;
        $form_state['response']['fields_in'] = $fields_in;
        $form_state['response']['fields_out'] = $fields_out;
    }
    else drupal_set_message($err);
}

/*********************************************************************************
  *                                                  Creating HTML Table                                                                       *
  *********************************************************************************/

function remotecck_create_operation_table($title, $operation) {
    //$table_information = array('Operation Name','Input Message Name','Input Parameter Name','Input Message Namespace','Output Message Name','Output Parameter Name','Output Message Namespace');
    $table_information = array('Operation Name','Input Message Name','Input Parameter Name','Output Message Name','Output Parameter Name');
    $num_of_rows = count($operation);
    $html_code = "";
    if($num_of_rows>0) {
        $html_code = "<table border = '3' width='10'><caption><em><b>$title</b></em></caption><tr>";
        $num_of_colomns = count($table_information);
        for($i = 0; $i < $num_of_colomns; $i++) {
            $html_code.= "<th bgcolor = '#C0C0C0'>".$table_information[$i]."</p></th>";
        }
        $html_code.="</tr>";
        for($i = 0; $i < $num_of_rows; $i++) {
            if($i%2==0) $color_code = "#EDF5FA";
            else $color_code = "#FFFFFF";
            $html_code.= "<tr>";
            $html_code.= "<th bgcolor = '$color_code' >".$operation[$i]["operation"]."</p></th>";
            $html_code.= "<th  bgcolor = '$color_code' >".$operation[$i]["input"]["messageName"]."</p></th>";
            $html_code.= "<th  bgcolor = '$color_code' >".$operation[$i]["input"]["parameterName"]."</p></th>";
            //$html_code.= "<th>".$operation[$i]["input"]["messageNamespace"]."</th>";
            $html_code.= "<th  bgcolor = '$color_code' >".$operation[$i]["output"]["messageName"]."</p></th>";
            $html_code.= "<th  bgcolor = '$color_code' >".$operation[$i]["output"]["parameterName"]."</p></th>";
            //$html_code.= "<th>".$operation[$i]["output"]["messageNamespace"]."</th>";
            $html_code.= "</tr>";
        }
        $html_code.= "</table>";
    }
    return $html_code;
}


function remotecck_create_fields_table($title, $fields) {
    //          1) $fields[$j]["operation"]
    //          2) $fields[$j]["message"]
    //          3) $fields[$j]["parameter"]
    //          4) $fields[$j]["fields"][$i]["name"]
    //               $fields[$j]["fields"][$i]["type"]
    //               $fields[$j]["fields"][$i]["namespace"]
    $table_information = array('Message Name', 'Operation Name', 'Name Field', 'Type Field');
    $num_of_operation = count($fields);
    $html_code = "";
    if($num_of_operation>0) {
        $html_code = "<table border = '3' width='10'><caption><em><b>$title</b></em></caption><tr>";
        $num_of_colomns = count($table_information);
        for($i = 0; $i < $num_of_colomns; $i++) {
            $html_code.= "<th bgcolor = '#C0C0C0'>".$table_information[$i]."</p></th>";
        }
        $html_code.="</tr>";
        for($i = 0; $i < $num_of_operation; $i++) {
            // the number of field fo operation
            if(($i)%2==0) $color_code = "#EDF5FA";
            else $color_code = "#FFFFFF";
            $num_of_field_operation = count($fields[$i]["fields"]);
            $num_of_field_operation_col = $num_of_field_operation+1;
            $html_code.= "<th rowspan = '$num_of_field_operation_col' bgcolor = '$color_code' >".$fields[$i]["message"]."</p></th>";
            $html_code.= "<th rowspan = '$num_of_field_operation_col' bgcolor = '$color_code' >$num_of_rows".$fields[$i]["operation"]."</p></th>";
            for($j = 0; $j < $num_of_field_operation; $j++) {
                if(($j+$i)%2==0) $color_code = "#EDF5FA";
                else $color_code = "#FFFFFF";
                $html_code.= "<tr>";
                $html_code.= "<th  bgcolor = '$color_code' >".$fields[$i]["fields"][$j]["name"]."</p></th>";
                $html_code.= "<th  bgcolor = '$color_code' >".$fields[$i]["fields"][$j]["type"]."</p></th>";
                $html_code.= "</tr>";
            }
        }
        $html_code.= "</table>";
    }
    return $html_code;
}
?>
