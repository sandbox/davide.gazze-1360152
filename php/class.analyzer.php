<?php

class Analyzer {

    private $wsdlURL;
    // The URL
    private $url;
    // all the namespace
    private $namespace;
    // the XML SCHEMA VERSIONE
    private $xsd;
    // the TNS Namespace
    private $tns;

    // TODO creating the getting function
    public function Analyzer($url) {
        // Constructor
        // return:
        //              OK    => 1
        //              error => otherwise
        $this->wsdlURL = new wsdl($url, 'wsdl');
        $err = $this->wsdlURL->getError();
    }

    public function getError() {
        if ($err) return $err;
        else {
            // getting the namespace
            $this->xsd = $this->wsdlURL->XMLSchemaVersion;
            $this->namespace = $this->wsdlURL->namespaces;
            $this->tns = $this->namespace["tns"];
            // save the url
            $this->url = $url;
            return "1";
        }
    }
    public function getOperations($type) {
        // Return the operation of WSDL url with the follow format:
        // For each operation
        //      1) $operation[$i]["operation"]
        //      2) $operation[$i]["input"]["parameterName"]
        //           $operation[$i]["input"]["messageName"]
        //           $operation[$i]["input"]["messageNamespace"]
        //      3) $operation[$i]["output"]["parameterName"]
        //           $operation[$i]["output"]["messageName"]
        //           $operation[$i]["output"]["messageNamespace"]
        $value = $this->wsdlURL->getOperations($type);
        $operation = array();
        $i = 0;
        foreach ($value as $key) {
            $operation[$i]["operation"] = $key["name"];
            $operation[$i]["input"] = array();
            $operation[$i]["output"] = array();
            // take the Input message
            $operation[$i]["input"]["message"] = $key['input']['message'];
            $paramArrayInput = each($key['input']['parts']);
            if($paramArrayInput!="") {
                $paramNameInput = $paramArrayInput["key"];
                $messageNameInput = $key['input']['parts'][$paramNameInput];
                $messageNameInputArray = $this->getInformation($messageNameInput);
                // the operation name is only the last part
                $messageNameInput = $messageNameInputArray[1];
                $messageNameInputNamespace = $messageNameInputArray[0];
            }
            else {
                $paramNameInput = "";
                $messageNameInput = "";
                $messageNameInputNamespace = "";
            }
            $operation[$i]["input"]["parameterName"] = $paramNameInput;
            $operation[$i]["input"]["messageName"] = $messageNameInput;
            $operation[$i]["input"]["messageNamespace"] = $messageNameInputNamespace;
            // take the output message
            $operation[$i]["output"]["message"] = $key['output']['message'];
            $paramArrayOutput = each($key['output']['parts']);
            if($paramArrayOutput!="") {
                // take the value
                $paramNameOutput = $paramArrayOutput["key"];
                $messageNameOutput = $key['output']['parts'][$paramNameOutput];
                $messageNameOutputArray = $this->getInformation($messageNameOutput);
                // the operation name is only the last part
                $messageNameOutput = $messageNameOutputArray[1];
                $messageNameOutputNamespace = $messageNameOutputArray[0];
            }
            else {
                $paramNameOutput = null;
                $messageNameOutput = null;
                $messageNameOutputNamespace = "";
            }
            $operation[$i]["output"]["parameterName"] = $paramNameOutput;
            $operation[$i]["output"]["messageName"] = $messageNameOutput;
            $operation[$i]["output"]["messageNamespace"] = $messageNameOutputNamespace;
            $i++;
        }
        return $operation;
    }

    public function getTypes($operationName, $messageInformation) {
        // get the field of a type and
        // return the fields components if exists with the follow format:
        //          1) $returnFields["operation"]
        //          2) $returnFields["message"]
        //          3) $returnFields["parameter"]
        //          4) $returnFields["fields"][$i]["name"]
        //               $returnFields["fields"][$i]["type"]
        //               $returnFields["fields"][$i]["namespace"]
        //           false othewise
        $returnFields = array();
        $returnFields["operation"] = $operationName;
        $returnFields["message"] = $messageInformation["messageName"];
        $returnFields["parameter"] = $messageInformation["parameterName"];
        $returnFields["fields"] = array();
        if(strpos($messageInformation["messageNamespace"], $this->xsd)===0) {
            // Base Type
            $returnFields["fields"][0]["name"] = $messageInformation["parameterName"];
            $returnFields["fields"][0]["type"] = $messageInformation["messageName"];
            $returnFields["fields"][0]["namespace"] = $messageInformation["messageNamespace"];
        }
        else {
            // Internal type search
            $name = $messageInformation["messageName"];
            $field_type = $messageInformation["messageNamespace"];
            $result = array();
            $j = 0;
            $this->getRealTypeField($name, $name, $field_type, $j, $result);
            $returnFields["fields"] = $result;
        }
        return $returnFields;
    }

    /******************************************************************************************
            *                                                                   Private Function                                                               *
            ******************************************************************************************/

    private function getInformation($str) {
        // divide string:
        //      aaa:bbbb:cccc
        // or
        //      aaaabbbb:cccc
        // the string in 2 part
        // [0] => "aaa:bbbb" or "aaaabbbb"
        // [1] =>  "cccc"
        // else return $str
        $strLenIndex = strlen($str)-1;
        // Check for ^ char and delete it
        if($str[$strLenIndex]=="^") $str = substr($str, 0, $strLenIndex);
        $last_position = strrpos($str,":");
        if($last_position!==false) {
            $len = strlen($str);
            $value = array();
            if($last_position!==false) {
                $value[0] = substr($str, 0, $last_position);
                $value[1] = substr($str, $last_position+1, $len-$last_position-1);
                return $value;
            }
        }
        else return $str;
    }

    private function getRealTypeField($field_name, $field_type, $namespace, &$j, &$result) {
        $fields = $this->getFields($field_name, $field_type, $namespace);
        //print("<br>");
        $num_of_field = count($fields);
        for($i=0; $i<$num_of_field; $i++) {
            if(isset($fields[$i]["type"])) {
                if(strpos($fields[$i]["namespace"],$this->xsd)===0) {
                    // ++$ is better than $++
                    $result[$j] = $fields[$i];
                    ++$j;
                }
                else if(strpos($fields[$i]["type"],$this->xsd)===0) {
                    // ++$ is better than $++
                    $type_array = $this->getInformation($fields[$i]["type"]);
                    $fields[$i]["type"] = $type_array[1];
                    $fields[$i]["namespace"] = $type_array[0];
                    $result[$j] = $fields[$i];
                    ++$j;
                }
                else {
                    // perhaps (strpos ($ this-> tns, "urn ")=== 0) useless
                    if((strpos($this->tns,"urn")===0) || (strpos($this->url,$this->tns)===0) || (strpos($fields[$i]["type"],$this->tns)===0) || (empty($this->tns))) {
                        $type_i_array = $this->getInformation($fields[$i]["type"]);
                        $fields[$i]["type"] = $type_i_array[1];
                        $fields[$i]["namespace"] = $type_i_array[0];
                    }
                    $this->getRealTypeField($fields[$i]["name"], $fields[$i]["type"], $fields[$i]["namespace"], &$j, &$result);
                }
            }
            else {
                print_r($fields);
                print("SOMETHING OF WRONG");
            }
        }
    }

    private function getFields($field_name, $field_type, $namespace) {
        // Get the all information for a field
        $fields = $this->wsdlURL->getTypeDef($field_type, $namespace);
        $type_array = "";
        $result = array();
        $i = 0;
        if($fields["phpType"]=="array") {
            $type_array = $this->getInformation($fields["arrayType"]);
            if(strpos($fields["arrayType"],$this->xsd)===0) {
                $result[0]["name"] = $fields["name"]."[]";
                $result[0]["type"] = $type_array[1];
                $result[0]["namespace"] = $this->xsd;
            }
            else {
                $result_array = $this->getFields($field_name, $type_array[1], $type_array[0]);
                $num_of_result_rec = count($result_array);
                for($z=0; $z<$num_of_result_rec; $z++) {
                    $result[$i]["name"] = $result_array[$z]["name"]."[]";
                    $result[$i]["type"] = $result_array[$z]["type"];
                    $result[$i]["namespace"] = $result_array[$z]["namespace"];
                    $i++;
                }
            }
        }
        else if(($fields["phpType"]=="struct") || ($fields["phpType"]=="scalar")) {
            $field_to_analyze = array();
            if(isset($fields["elements"])) {
                $field_to_analyze = $fields["elements"];
            }
            else if(isset($fields["attrs"])) {
                $field_to_analyze = $fields["attrs"];
            }
            else {
                $field_to_analyze[$field_name] = $fields;
            }
            foreach ($field_to_analyze as $key => $field) {
                $result_i = $this->getFieldsInformation($key, $field_type, $field);
                $result[$i] = $result_i;
                if(!empty($result_i)) {
                    if($result[$i]["namespace"]=="REC") {
                        // insert recursive field
                        $result_rec = $this->getFields($field_name, $this->tns.":".$field_type, $namespace);
                        $num_of_result_rec = count($result_rec);
                        for($z=0; $z<$num_of_result_rec; $z++) {
                            $result[$i]["name"] = $result_rec[$z]["name"];
                            $result[$i]["type"] = $result_rec[$z]["type"];
                            $result[$i]["namespace"] = $result_rec[$z]["namespace"];
                            $i++;
                        }
                    }
                    $i++;
                }
            }
        }
        else {
            print_r($fields);
            die("getFields : VUOTO");
        }
        return $result;
    }

    private function getFieldsInformation($field_name, $field_type, $field) {
        // empty field
        if((is_array($field)) && (isset($field["abstract"])) && ($field["abstract"]==true)) return false;
        // not empty field
        $result = array();
        // Get the singular information
        $result["namespace"] = "";
        if((is_array($field)) && (isset($field["name"]))) {
            $result["name"] = $field["name"];
        }
        else {
            $result["name"] = $field_name;
        }
        if(isset($field["type"])) {
            $result["type"] = $field["type"];
        }
        else if(isset($field["ref"])) {
            $result["type"] = $field["ref"];
        }
        else if(isset($field["phpType"])) {
            // for scalar type
            if($field["phpType"]=="scalar") {
                $result["type"] = "Scalar";
                $result["namespace"] = "http://www.w3.org/2001/XMLSchema";
            }
            else {
                $result["name"] = $field_name;
                $result["type"] = $this->tns.":".$field_type;
                $result["namespace"] = "REC";
            }
        }
        else {
            die("VUOTO-->getFieldsInformation(1)");
        }
        return $result;
    }
}

?>