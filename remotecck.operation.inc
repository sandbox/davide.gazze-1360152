<?php
// insert WSDL

function remotecck_operation(&$form_state = NULL) {
    global $user;
    if($user->uid != 1) {
        // only the admin can use remotecck
        drupal_set_title('Access denied');
        drupal_set_message("You are not authorized to access this page.","error");
        return "";
    }
    // making the form
    $form = array();
    // insert the content type
    $form['name_of_cck'] = array(
        '#title'            => t('Name of Content Type'),
        '#type'             => 'textfield',
        '#weight'           => $weight++,
        '#size'             => 62,
        '#description'      => t("Every character '.' will change with '_' ")
    );
    // getting the list of WSDL
    $wsdl_list[0] = 'Select options';
    $wsdl_list = array_merge($wsdl_list, remotecck_get_WSDL());
    $form['wsdl'] = array(
        '#type'             => 'select',
        '#weight'           => $weight++,
        '#title'            => t('Web Services List'),
        '#options'          => $wsdl_list,
        '#description'      => t("If you don't have any Web Services click <a href=!url>here</a>.", array('!url' => url("admin/settings/remotecck/insert", array('absolute' => TRUE)))),
        '#attributes'       => array(
            'onchange'           => 'get_operation("edit-wsdl","edit-operation-wsdl","operation_wsdl_div","edit_operation_select","fields_operation_wsdl_div","edit_field_operation","edit-name-of-cck","edit_field_name", 1, true);')
    );
    $form['operation_wsdl_insert'] = array(
        '#type'             => 'item',
        '#weight'           => $weight++,
        '#prefix'           => '<div id="operation_wsdl_div">',
        '#suffix'           => '</div>'
    );
    $form['operation_wsdl'] = array(
        '#type'             => 'hidden',
        '#weight'           => $weight++
    );
    $form['fields_operation_wsdl'] = array(
        '#type'             => 'hidden',
        '#weight'           => $weight++,
        '#prefix'           => '<div id="fields_operation_wsdl_div">',
        '#suffix'           => '</div>'
    );
    if(isset($form_state['post']['edit_field_operation'])) {
        $num_fields = $form_state['post']['edit_field_operation'];
        // saving the post information
        if($num_fields>0) {
            $form['edit_field_operation'] = array(
                '#type'             => 'hidden',
                '#value'            => $form_state['post']['edit_field_operation']
            );
            for($i=0; $i<$num_fields; $i++) {
                // Insert field
                $form['edit_field_operation'.$i] = array(
                    '#type'             => 'hidden',
                    '#value'            => $form_state['post']['edit_field_operation'.$i]
                );
                $form['edit_field_name'.$i] = array(
                    '#type'             => 'hidden',
                    '#value'            => $form_state['post']['edit_field_name'.$i]
                );
            }
        }
    }
    $form['submit'] = array(
        '#type'             => 'submit',
        '#weight'           => $weight++,
        '#value'            => t('Create new Content Type')
    );
    return $form;
}

function remotecck_operation_validate($form, &$form_state) {
    $operation_name = $form_state['values']['operation_wsdl'];
    $operation_name_cck = $form_state['values']['name_of_cck'];
    $wsdl_url = $form_state['values']['wsdl'];
    $operation_info = remotecck_get_operation_id($wsdl_url, $operation_name);
    // Eliminate the can't use character
    $search  = array('.');
    $replace = array('_');
    $operation_name_cck = str_replace($search, $replace, $operation_name_cck);
    $operation_name_cck_old = $operation_name_cck;
    $content_types = content_copy_types();
    $content_types = array_values($content_types);
    $num_of_content = count($content_types);
    $error = false;
    for($i=0; $i<$num_of_content; $i++) {
        if(strpos($content_types[$i], $operation_name_cck)===0) {
            $error = true;
            // Setting the new name of content type
            // as $operation_name_cck and the id of operation
            if(isset($operation_info["id"])) {
                // It may be impossible that not exist the operation_name
                $operation_name_cck = $operation_name_cck . $operation_info["id"];
            }
            else {
                // No cck Found
                // Internal Error
                $operation_name_cck = $operation_name_cck . "1";
            }
            $form_state['values']['name_of_cck'] = $operation_name_cck;
            // Recheck for every content_type
            $i = 0;
        }
    }
    // Setting the error
    if($error) {
        // Update the information
        drupal_add_js(
            "$(document).ready(function(){
            $('#edit-name-of-cck').val('$operation_name_cck');
            });",
            "inline"
        );
        form_set_error('name_of_cck',t('The content type <b>'.$operation_name_cck_old.'</b> already exist.<br/>It will changed in <b>'.$operation_name_cck.'</b>.'));
    }
    // Validating the fields name
    $num_fields = $form_state['values']['edit_field_operation'];
    for($i=0; $i<$num_fields; $i++) {
        // Check for duplicate name of file
        $error = false;
        $fields_insert = $form_state['values']['edit_field_operation'.$i];
        $field_name = "field_".strtolower($fields_insert);
        $field_name = remotecck_validate_field_name($field_name);
        // Saving the fieldname
        $form_state['values']['edit_field_name'.$i] = $field_name;
    }
}

function remotecck_operation_submit($form, &$form_state) {
    // Submit the Content Type
    remotecck_operation_mapping($form, &$form_state, true);
}

?>
