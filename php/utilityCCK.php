<?php

/******************************************************************************
                             CCK Management
 ******************************************************************************/

function remotecck_get_content_fields($content_type, $all_info = false) {
    // return an associative array of all field for the content type
    // if tables_empty is false all the content type can pass
    // otherwise pass only the content type with the field["table"] not empty
    // if it's passed all_info = true return all the infomation taken from remotecck_field_mapping
    // and check for present field
    // check for field "all_fields_present" that say if all field has associated an operation
    // return in the follow format
    //    ["names"][$field_name] = name of fields
    //    ["fields"][$content_type_id][$field_name] = all information of each field
    //                     content_type_id
    //                     id_message
    //                     field_parameter
    //                     type_parameter
    //                     message_name
    //                     direction_message
    //                     parameter_name
    //                     id_operation
    $fields_of_content_type = content_copy_fields($content_type);
    $fields = array();
    $fields["names"] = $fields_of_content_type;
    if($all_info) {
        // Getting the operation list
        $fields["fields"] = array();
        $number_of_fields_content_type = count($fields_of_content_type);
        for($i=0; $i<$number_of_fields_content_type; $i++) {
            // check for empty content type tables
            // NOT DONE YET
            $field_name = $fields_of_content_type[$i];
            $field_information = remotecck_field_mapping($field_name);
            $num_of_fields = count($field_information);
            for($j=0; $j<$num_of_fields; $j++) {
                if(!empty($field_information[$j])) {
                    $key = $field_information[$j]['content_type_id'];
                    if($key !== false) {
                        $fields["fields"][$key][$field_name] = $field_information[$j];
                    }
                    else {
                        // If the database is integry this is not possible
                        // for now return false
                        return false;
                    }
                }
            }
        }
    }
    return $fields;
}

function remotecck_create_field($fields_information, $operation_information) {
    // return a list of all field for the content type
    // separate in input and output
    // and for operation
    // if tables_empty is false all the content type can pass
    // otherwise pass only the content type with the field["table"] not empty
    $num_input_param = 0;
    $num_output_param = 0;
    $in_str = "IN";
    $out_str = "OUT";
    $in_index = 0;
    $out_index = 0;
    $fields_json = array();
    $fields_json["items"] = array();
    foreach($fields_information['fields'][$operation_information["id"]] as $key => $field) {
        $direction = $field["direction_message"];
        if(strcmp($direction , $in_str)==0) {
            $num_input_param++;
            $fields_json["items"]["input"][$in_index] = $key;
            $in_index++;
        }
        else if(strcmp($direction , $out_str)==0) {
            $num_output_param++;
            $fields_json["items"]["output"][$out_index] = $key;
            $out_index++;
        }
    }
    $fields_json["num_input_param"] = $num_input_param;
    $fields_json["num_output_param"] = $num_output_param;
    return drupal_to_js($fields_json);
}

function remotecck_check_field($field_name) {
    // return the name of the content type if a field is present
    // false otherwise
    $content_types = content_copy_types();
    foreach($content_types as $key => $value) {
        $fields = content_copy_fields($key);
        $num_of_fields = count($fields);
        for($i=0; $i<$num_of_fields; $i++)
            if($fields[$i]===$field_name) return $key;
    }
    return false;
}

?>