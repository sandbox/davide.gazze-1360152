function get_information_for_content_type(name_of_cck, connection_div, name_of_select, connection_select) {
    // Get the fields of operation of WSDL
    content_type = $('#'+name_of_cck).val();
    data_send = "content_type="+content_type;
    $.ajax({type: "POST", url:"?q=remotecck.get.connection.search", async:true, data:data_send, success:function(data) {
            operation_json = eval('(' + data + ')');
            select_code = "<select id='"+name_of_select+"' onChange=\"$('#"+connection_select+"').val(this.value);\"/>";
            number_of_fields = $(operation_json).length;
            obj = $("#"+connection_div);
            obj.empty();
            obj.append("<b>Connection: Operation (URL)</b><br/>");
            obj.append(select_code);
            // remove all
            obj_select = $("#"+name_of_select);
            obj_select.empty();
            // insert the connection
            for(i=0; i<number_of_fields; i++) {
                connection_to_insert = operation_json[i].operation_name + " (" + operation_json[i].wsdl_url + ")";
                option = new Option(connection_to_insert, connection_to_insert);
                obj_select.append(option);
            }
            // select the first one connection
            first_connection_to_insert = operation_json[0].operation_name + " (" + operation_json[0].wsdl_url + ")";
            $('#'+connection_select).val(first_connection_to_insert);
        }
    });
}