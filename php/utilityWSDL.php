<?php

/******************************************************************************
                             WSDL Management
 ******************************************************************************/
 
function remotecck_insertWSDL($url="", $soap_version="" ,$login="" ,$password="" ,$proxy_host="" ,$proxy_port="" ,$proxy_login="" ,$proxy_password="" ,$local_cert="" ,$compression="" ,$encoding="") {
    // insert the wsdl information into Database
    // TODO check if $url exists
    // Check for duplicate WSDL
    // and EXIT if found it
    // otherwise continue
    $sql_Exit = "SELECT max(id) FROM remotecck_wsdl_info WHERE url='%s'";
    $result = db_result(db_query($sql_Exit, $url));
    if($result!=0) {
        return -1;
    }
    $sqlInsert="INSERT INTO remotecck_wsdl_info (id, url, soap_version, login, password, proxy_host, proxy_port, proxy_login, proxy_password, local_cert, compression, encoding) VALUES (NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
    if (!db_query($sqlInsert, $url, $soap_version,$login,$password,$proxy_host,$proxy_port,$proxy_login,$proxy_password,$local_cert,$compression,$encoding)) {
            drupal_set_message('Error Insert WSDL');
    }
    //print "<br>WSDL ADDED";
    return 0;
}

function remotecck_insertOperation($url, $operation, $type) {
    // Insert Operation Information into Database
    // $url = url of WSDL
    // $operation = associative array of every operation
    // $type = type of operation (SOAP, SOAP1.2)
    $sql_WSDL_Index = "(SELECT max(id) FROM remotecck_wsdl_info WHERE url='".$url."')";
    // Check for duplicate operation in WSDL URL
    $sql_Exit = "SELECT max(id) FROM remotecck_operation WHERE operation_name='%s' AND id_wsdl_info = $sql_WSDL_Index";
    // Insert Operation Query
    $sqlInsOperation="INSERT INTO remotecck_operation (id, id_wsdl_info, operation_name, operation_type) VALUES (NULL,$sql_WSDL_Index,'%s','%s')";
    // ID Operation Query
    $sqlOperationBaseID = "(SELECT max(id) FROM remotecck_operation WHERE operation_name='".$operation[$i]["operation"]."')";
    // Insert Message Query
    $sqlInsMessage="INSERT INTO remotecck_message (id, id_operation, message_name, parameter_name, direction_message) VALUES (NULL,(SELECT max(id) FROM remotecck_operation WHERE operation_name='%s'),'%s','%s','%s')";
    for($i = 0; $i < count($operation); $i++) {
        // Check for duplicate Operation
        // and EXIT if found it
        // otherwise continue to Add
        $result = db_result(db_query($sql_Exit, $operation[$i]["operation"]));
        if($result==0) {
            // Insert the operation
            if(($operation[$i]["input"]["messageName"]!="") && ($operation[$i]["output"]["messageName"]!="")) {
                if (!db_query($sqlInsOperation,$operation[$i]["operation"],$type)) {
                    drupal_set_message('Error Insert Operation');
                }
                else {
                    // Insert the input message
                    if (!db_query($sqlInsMessage,$operation[$i]["operation"],$operation[$i]["input"]["messageName"],$operation[$i]["input"]["parameterName"],'IN')) {
                        drupal_set_message('Error Insert OUTPUT');
                    }
                    // Insert the output message
                    if (!db_query($sqlInsMessage,$operation[$i]["operation"],$operation[$i]["output"]["messageName"],$operation[$i]["output"]["parameterName"],'OUT')) {
                        drupal_set_message('Error Insert OUTPUT');
                    }
                }
            }
            else {
                // Echo request msg TO DO
            }
        }
    }
}

function remotecck_insertFields($fields) {
    // Insert Field INTO Database
    // $fields = associative array of information
    // TODO check if $field exist
    $sqlMessage = "(SELECT max(id) FROM remotecck_message WHERE message_name='".$fields["message"]."' AND parameter_name = '".$fields["parameter"]."' AND id_operation = (SELECT max(id) FROM remotecck_operation WHERE operation_name='".$fields["operation"]."'))";
    $sqlInsert = "INSERT INTO remotecck_parameter (id, id_message, field_parameter, type_parameter) VALUES (NULL,".$sqlMessage.",'%s','%s')";
    $num = count($fields["fields"]);
    for($i=0; $i<$num ; $i++) {
        if (!db_query($sqlInsert,$fields["fields"][$i]["name"],$fields["fields"][$i]["type"]))
            drupal_set_message("Error Field");
    }
}

function remotecck_get_WSDL() {
    // getting all WSDL from Database
    $sql_WSDL = "SELECT url FROM remotecck_wsdl_info";
    $result = db_query($sql_WSDL);
    $wsdl_list = array();
    while ($data = db_fetch_object($result)) {
        $url = $data->url;
        $wsdl_list[$url] = $url;
    }
    return $wsdl_list;
}

function remotecck_get_WSDL_operation($wsdl, $check_used = 1) {
    // get the operation, message and fields
    // if $ckeck_duplicate is true, the function check for duplicates
    $i = 0;
    $sql_operation = "SELECT operation_name FROM remotecck_operation WHERE id_wsdl_info = (SELECT MAX(id) FROM remotecck_wsdl_info WHERE url = '%s')";
    $result = db_query($sql_operation, $wsdl);
    $operation_list = array();
    while ($data = db_fetch_object($result)) {
        $operation_name = $data->operation_name;
        // The real condition for insert is:
        // ($check_used == 1) && remotecck_check_connection_operation = false
        // or
        // ($check_used == 0)
        // semplified like this
        if(!(($check_used==1) && remotecck_check_connection_operation($operation_name, $wsdl))) {
            $operation_list[++$i] = $operation_name;
        }
    }
    return $operation_list;
}

function remotecck_get_operation_fields($wsdl, $operation) {
    // get the operation, message and fields
    // with the information:
    //     1) id_field
    //     2) field_name
    //     3) field_type
    //     4) direction
    $field_list = array();
    $sql_operation = "SELECT parameter.id, parameter.id_message, parameter.field_parameter, parameter.type_parameter, message.direction_message FROM remotecck_parameter AS parameter INNER JOIN remotecck_message AS message ON (parameter.id_message = message.id) WHERE (id_operation = (SELECT MAX(id) FROM remotecck_operation WHERE operation_name = '%s' AND id_wsdl_info = (SELECT MAX(id) FROM remotecck_wsdl_info WHERE url = '%s')))";
    $result = db_query($sql_operation, $operation, $wsdl);
    $i = 0;
    while ($data = db_fetch_object($result)) {
        $field_list[$i]["id_field"]     = $data->id;
        $field_list[$i]["id_message"]   = $data->id_message;
        $field_list[$i]["field_name"]   = $data->field_parameter;
        $field_list[$i]["field_type"]   = $data->type_parameter;
        $field_list[$i]["direction"]    = $data->direction_message;
        $i++;
    }
    return $field_list;
}

function remotecck_field_mapping($field_name) {
    // Return if is present the corrispondent :
    //      content_type_id
    //      id_message
    //      field_parameter
    //      type_parameter
    //      message_name
    //      direction_message
    //      parameter_name
    //      id_operation
    //the max clausule is for erroneous duplicate value (impossible if remotecck is used correctly)
    $sql_field = "SELECT mapping.cck_operation_id, mapping.field_name, parameter.id_message, parameter.id, parameter.field_parameter, parameter.type_parameter, message.message_name, message.direction_message, message.parameter_name, message.id_operation FROM (remotecck_mapping AS mapping, remotecck_parameter AS parameter, remotecck_message AS message) WHERE (parameter.id = mapping.parameter_id  AND message.id = parameter.id_message AND mapping.field_name = '%s')";
    $result = db_query($sql_field,$field_name);
    $result_array = array();
    $i = 0;
    while($data = db_fetch_object($result)) {
        // Suppose that it's impossible having multiple result
        // if it's used correctly the database
        /************************************************************
                            Further development : Check for error in database
                     ************************************************************/
        $result_array[$i]["content_type_id"]    = $data->cck_operation_id;
        $result_array[$i]["id_message"]         = $data->id_message;
        $result_array[$i]["field_parameter"]    = $data->field_parameter;
        $result_array[$i]["type_parameter"]     = $data->type_parameter;
        $result_array[$i]["message_name"]       = $data->message_name;
        $result_array[$i]["direction_message"]  = $data->direction_message;
        $result_array[$i]["parameter_name"]     = $data->parameter_name;
        $result_array[$i]["id_operation"]       = $data->id_operation;
        $result_array[$i]["id_field"]           = $data->id;
        $result_array[$i]["field_name"]         = $data->field_name;
        $i++;
    }
    return $result_array;
}

function remotecck_insert_mapping($id_content_type, $id, $field_name) {
    // Insert a mapping between id and field_name
    $sqlInsert = "INSERT INTO remotecck_mapping (cck_operation_id, parameter_id, field_name) VALUES (%d, %d,'%s')";
    if(!db_query($sqlInsert,$id_content_type, $id, $field_name)) {
        drupal_set_message("Error Field");
        return 0;
    }
    return 1;
}

function remotecck_saving_new_cck($content_type, $operation, $url, $insert_mode = 2) {
    // Insert the new content type in the cck log
    // and return the last id insert
    $id = remotecck_get_operation_id($url, $operation);
    $sql_insert = "INSERT INTO remotecck_cck_created (id, content_type, id_operation, insert_mode) VALUES (NULL, '%s', %d, %d)";
    if(!db_query($sql_insert, $content_type, $id["id"], $insert_mode)) {
        drupal_set_message("Error Field");
    }
    else return remotecck_get_saving_id($content_type, $id);
}

function remotecck_get_saving_id($content_type, $id) {
    // return the id
    $sql_search_id = "SELECT MAX(id) AS 'id' FROM remotecck_cck_created WHERE content_type = '%s' AND id_operation = %d";
    $result = db_query($sql_search_id, $content_type, $id["id"]);
    $data = db_fetch_object($result);
    return $data->id;
}

function remotecck_getting_insert_cck() {
    // Getting the Content Type insert
    // as array with the follow information:
    // 1) content_type
    // 2) operation_name
    // 3) wsdl_url
    // 4) insert_mode
    $content_types = array();
    $sql_getting = "SELECT cck_created.id, cck_created.insert_mode, cck_created.content_type, operation.operation_name, wsdl_info.url FROM remotecck_cck_created AS cck_created INNER JOIN remotecck_operation AS operation ON (operation.id = cck_created.id_operation) INNER JOIN remotecck_wsdl_info AS wsdl_info ON (wsdl_info.id = operation.id_wsdl_info)";
    $result = db_query($sql_getting);
    if($result) {
        $i = 0;
        while($data = db_fetch_object($result)) {
            $content_types[$i]["content_type"]   = $data->content_type;
            $content_types[$i]["operation_name"] = $data->operation_name;
            $content_types[$i]["wsdl_url"]       = $data->url;
            $content_types[$i]["insert_mode"]    = $data->insert_mode;
            ++$i;
        }
    }
    return $content_types;
}

function remotecck_getting_information_content_type($content_type) {
    // Getting all the information about every Operation connected to a Content Type
    // 1) content_type
    // 2) operation_name
    // 3) wsdl_url
    // 4) insert_mode
    $information = array();
    $sql_getting = "SELECT cck_created.id, cck_created.insert_mode, cck_created.content_type, operation.operation_name, wsdl_info.url FROM remotecck_cck_created AS cck_created INNER JOIN remotecck_operation AS operation ON (operation.id = cck_created.id_operation) INNER JOIN remotecck_wsdl_info AS wsdl_info ON (wsdl_info.id = operation.id_wsdl_info) WHERE cck_created.content_type = '%s'";
    $result = db_query($sql_getting, $content_type);
    if($result) {
        $i = 0;
        while($data = db_fetch_object($result)) {
            $information[$i]["id"]              = $data->id;
            $information[$i]["content_type"]    = $data->content_type;
            $information[$i]["operation_name"]  = $data->operation_name;
            $information[$i]["wsdl_url"]        = $data->url;
            $information[$i]["insert_mode"]     = $data->insert_mode;
            ++$i;
        }
    }
    return $information;
}

function remotecck_get_operation_id($wsdl_url, $operation) {
    // Getting the follow operation:
    // 1) id = id of operation
    // 2) id_wsdl_info = id of wsdl
    // 3) operation_name = name of operation
    // 4) operation_type = type of operation (soap right now)
    $information = array();
    $sql_search = "SELECT operation.id, operation.id_wsdl_info, operation.operation_name, operation.operation_type FROM remotecck_operation AS operation INNER JOIN remotecck_wsdl_info AS wsdl_info ON (operation.id_wsdl_info = wsdl_info.id AND wsdl_info.url = '%s') WHERE operation.operation_name = '%s'";
    $result = db_query($sql_search, $wsdl_url, $operation);
    $data = db_fetch_object($result);
    if($data) {
        $information["id"]             = $data->id;
        $information["id_wsdl_info"]   = $data->id_wsdl_info;
        $information["operation_name"] = $data->operation_name;
        $information["operation_type"] = $data->operation_type;
    }
    return $information;
}

function remotecck_alter_contentcck($id, $insert_mode) {
    // Modify the insert_mode
    // return the result of db_query
    $information = array();
    $sql_modify = "UPDATE remotecck_cck_created SET insert_mode = %d WHERE id = %d";
    $result = db_query($sql_modify, $insert_mode, $id);
    return $result;
}

function remotecck_check_connection_cck($cck_name) {
    // Check if a Content Type is present in remotecck
    // return true if it's present
    //             false otherwise
    $sql_search = "SELECT COUNT(*) FROM remotecck_cck_created WHERE content_type = '%s'";
    $result = db_result(db_query($sql_search, $cck_name));
    if($result!=0) return true;
    return false;
}

function remotecck_check_connection_operation($operation_name, $wsdl_url) {
    // Check if a Content Type is present in remotecck
    // return true if it's present
    //             false otherwise
    $id = remotecck_get_operation_id($wsdl_url, $operation);
    $sql_search = "SELECT COUNT(*) FROM remotecck_cck_created WHERE id_operation = %d";
    $result = db_result(db_query($sql_search, $id["id"]));
    if($result!=0) return true;
    return false;
}

function remotecck_delete_connection($content_type, $operation_name, $wsdl_url) {
    // Delete a connection between content type and operation
    // Return:
    //      true  = ALL OK
    //      false = Problem
    $id_operation = remotecck_get_operation_id($wsdl_url, $operation_name);
    $id = remotecck_get_saving_id($content_type, $id_operation["id"]);
    $sql_delete = "DELETE FROM remotecck_cck_created WHERE content_type = '%s' AND id_operation = %d";
    if(!db_query($sql_delete, $content_type, $id_operation["id"])) {
        //return 0;
    }
    $sql_delete = "DELETE FROM remotecck_mapping WHERE cck_operation_id = %d";
    db_query($sql_delete, $id);
    return 1;
}

/******************************************************************************
                     Operation for Validation and Submit
 ******************************************************************************/
 
function remotecck_operation_mapping($form, &$form_state, $is_new_content_type) {
    // if $is_new_content_type is true i create a new content type else i update it
    // Submit the Content Type
    $url = $form_state['values']['wsdl'];
    $operation = $form_state['values']['operation_wsdl'];
    $operation_name_cck = $form_state['values']['name_of_cck'];
    $num_fields = $form_state['values']['edit_field_operation'];
    // Retrieve the information of operation
    $fields = remotecck_get_operation_fields($url, $operation);
    $fields_insert = array();
    $label = array();
    // Get the label and field_name
    for($i=0; $i<$num_fields; $i++) {
        $label[$i] = $form_state['values']['edit_field_operation'.$i];
        $fields[$i]["field_name"] = $form_state['values']['edit_field_name'.$i];
    }
    remotecck_insert_mapping_content_type($url, $operation, $operation_name_cck, $fields, $label, 2, $is_new_content_type);
}

function remotecck_insert_mapping_content_type($url, $operation, $operation_name_cck, $fields, $label, $insert_mode = 2, $is_new_content_type = false) {
    // Perform the real insert of operation into Content Type
    // Insert the mapping into Database
    // Create/Update the Content Type
    $num_of_insert = 0;
    // Insert the new content type in table
    $id_insert = remotecck_saving_new_cck(strtolower($operation_name_cck), $operation, $url, $insert_mode);
    // insert the mapping
    $num_fields = count($fields);
    for($i=0; $i<$num_fields; $i++) {
        $id = $fields[$i]["id_field"];
        $num_of_insert += remotecck_insert_mapping($id_insert, $id, $fields[$i]["field_name"]);
    }
    //if $num_of_insert<$num_fields some duplicate and you must erase all
    // Saving the new content type
    // two manner like that
    if(($num_of_insert==$num_fields) && ($num_fields>0)) {
        // Insert the cck description
        remotecck_insert_content_type($operation_name_cck, $operation, $fields, $label, $is_new_content_type);
        if($is_new_content_type) drupal_set_message("Creation of the Content Type $operation_name_cck done");
        else drupal_set_message("Update of the Content Type $operation_name_cck done");
    }
    else {
        if($num_fields==0) {
            // Write the code of delete
            drupal_set_message("Operation with no Fields");
        }
        else {
            // Write the code of delete
            drupal_set_message("Duplicate CCK");
        }
    }
    $form_state = array();
}

function remotecck_insert_content_type($operation_name_cck, $operation, $fields, $label, $is_new_content_type) {
    // Create or update a content type
    $content_obj = new Create_content_type($operation_name_cck, $operation, $fields, $label);
    $content = $content_obj->getContent();
    $form_state = array();
    $form = content_copy_import_form($form_state);
    // or like that
    include_once('./'. drupal_get_path('module', 'content') .'/includes/content.admin.inc');
    include_once('./'. drupal_get_path('module', 'node') .'/content_types.inc');
    // Insert the Values
    $form_state['values']['type_name'] = ($is_new_content_type) ? '<create>' : $operation_name_cck;
    $form_state['values']['macro'] = '$content = '. var_export($content, 1) .';';
    $form_state['rebuild'] = TRUE;
    content_copy_import_form_submit($form, &$form_state);
}

function remotecck_validate_field_name($field_name) {
    // Check for the field name
    $insert_number = 1;
    $is_first = true;
    $max_lenght = 32;
    do {
        // Check if the the field is too long
        // Now is 32
        // The str_len is the fields_insert + 6 because the field_name has the prefix "field_"
        $str_len = strlen($field_name);
        if($str_len>$max_lenght) {
            $field_name = substr($field_name, 0 , $max_lenght);
            $str_len = $max_lenght;
        }
        $content_name = remotecck_check_field($field_name);
        if($content_name!==false) {
            $error = true;
            // Calculate the number of char to insert
            if(!$is_first) {
                $number_of_removal_char_old = floor(log10($insert_number));
                $number_of_removal_char_new = floor(log10($insert_number+1));
                $diff = $number_of_removal_char_new - $number_of_removal_char_old;
                // The field name is too long to insert the operation_id
                // remove the last id
                $num_of_delete = -1*($diff+1);
                $field_name = substr($field_name, 0 , $num_of_delete);
            }
            else {
                $is_first = false;
            }
            $field_name = $field_name . $insert_number;
            $insert_number++;
        }
        else {
            // return the new field_name
            return $field_name;
        }
    } while(1);
}

?>
